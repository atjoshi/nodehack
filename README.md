Steps to deploy it on Heroku
-----------------------------
1. Clone the repo
2. Create heroku app : heroku create <appname>
3. Add git remote location to your heroku app : heroku git:remote -a <appname>
   
   Note : Now heroku remote will point to that repo with that branch.
   set git remote heroku to https://git.heroku.com/nodehack-atul-stage.git

4. Push the repo branch to heroku : git push heroku <git_branchname>
5. Scale your heroku app to the number of dynos available : heroku ps:scale web=1
   Note : Your have "Procfile" if you are deploying Node.js application
   Content => web: node server/server.js

6. To open you app : heroku open
7. To deploy master branch : git push heroku master
8. To deploy non-master branch : git push heroku <branchname>:master

NOTE: The code that is used to deploy(git push heroku <branchname>:master) doesn't deploy it to that original git repo branch it pushed to heroku's git branch and same for master branch.